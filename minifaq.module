<?php

$path = drupal_get_path('module', 'minifaq').'/minifaq.css';
drupal_add_css($path, $type = 'module', $media = 'all', $preprocess = TRUE);

/**
 * Implementation of hook_perm().
 */
function minifaq_perm() {
  return array('create faqs','view faqs');
	}  
 
/**
 * Implementation of hook_menu().
 */
function minifaq_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/minifaq',
      'title' => t('Minifaq Settings'),
      'description' => t('Configure Minifaq Module'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('minifaq_admin_settings'),
      'access' => user_access('administer site configuration')
    );
	$items[] = array(
      'path' => 'admin/settings/minifaq/add',
      'title' => t('Add'),
      'description' => t('Add FAQ'),
	  'callback' => 'drupal_get_form',
	  'callback arguments' => array('minifaq_add'),
      'access' => user_access('administer site configuration'),
	  'type' => MENU_LOCAL_TASK,
	  'weight' => -9,
    );
	$items[] = array(
      'path' => 'admin/settings/minifaq/list',
      'title' => t('List'),
      'description' => t('List FAQs'),
      'callback' => 'minifaq_list',
      'access' => user_access('administer site configuration'),
	  'type' => MENU_LOCAL_TASK,
	  'weight' => -10,
    );
	$items[] = array(
      'path' => 'admin/settings/minifaq/delete',
      'callback' => 'minifaq_delete',
      'access' => user_access('administer site configuration'),
    );
	$items[] = array(
      'path' => 'admin/settings/minifaq/edit',
      'callback' => 'drupal_get_form',
	  'callback arguments' => array('minifaq_edit'),
      'access' => user_access('administer site configuration'),
    );
	}else{ 
	$items[] = array(
      'path' => variable_get('minifaq_path','faqs'),
      'title' => variable_get('minifaq_title','Frequently Asked Questions'),
      'callback' => 'minifaq_page',
      'access' => user_access('view faqs'),
    );   
  }		
  return $items;
}

/**
 * Define the settings form.
 */
function minifaq_admin_settings() {
  $form['minifaq_path'] = array(
    '#type' => 'textfield',
    '#title' => t('FAQ path'),
    '#default_value' => variable_get('minifaq_path','faqs'),
    '#description' => t('Set the path for the minifaq page. This is useful if you are only providing FAQs for a subsection of your site.'),
  );
  $form['minifaq_title'] = array(
    '#type' => 'textfield',
    '#title' => t('FAQ page title'),
    '#default_value' => variable_get('minifaq_title','Frequently Asked Questions'),
    '#description' => t('Set the title for the minifaq page.'),
  );
  $form['array_filter'] = array('#type' => 'hidden');
  return system_settings_form($form);
}

/**
 * Define the FAQ form.
 */
function minifaq_add() {
  $form['minifaq_question'] = array(
    '#type' => 'textfield',
    '#title' => t('Question'),
    '#required' => TRUE,
    '#description' => t('Question'),
  );
  $form['minifaq_answer'] = array(
    '#type' => 'textarea',
    '#title' => t('Answer'),
    '#required' => TRUE,
    '#description' => t('Answer'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
	'#value' => t('Create'),
	);
  return $form;
}

function minifaq_add_submit($form, $form_values) {
	$result = db_query("INSERT INTO {minifaq_faq} (faqid, question, answer) VALUES (%d, '%s', '%s')",db_next_id('{minifaq_faq}_id'),$form_values['minifaq_question'],$form_values['minifaq_answer']);
	if ($result === FALSE) {
	  $message = t('Failed to create new FAQ. Please contact the administrator');
	  drupal_set_message($message, $type = 'error');
	  } else {
	  $message = t('New FAQ created');
	  drupal_set_message($message, $type = 'status');
	  }
	}
	
function minifaq_edit($faqid) {
  $faq = db_fetch_object(db_query("SELECT * FROM {minifaq_faq} WHERE faqid = %d",$faqid));
  $form['minifaq_edit_question'] = array(
    '#type' => 'textfield',
    '#title' => t('Question'),
    '#required' => TRUE,
    '#description' => t('Question'),
	'#default_value' => $faq->question,
  );
  $form['minifaq_edit_answer'] = array(
    '#type' => 'textarea',
    '#title' => t('Answer'),
    '#required' => TRUE,
    '#description' => t('Answer'),
	'#default_value' => $faq->answer,
  );
  $form['minifaq_edit_faqid'] = array(
    '#type' => 'value',
	'#value' => $faq->faqid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
	'#value' => t('Update'),
	);
  return $form;
}

function minifaq_edit_submit($form, $form_values) {
	$result = db_query("UPDATE {minifaq_faq} SET question = '%s', answer = '%s' WHERE faqid = %d",$form_values['minifaq_edit_question'],$form_values['minifaq_edit_answer'], $form_values['minifaq_edit_faqid']);
	if ($result === FALSE) {
	  $message = t('Failed to update FAQ. Please contact the administrator');
	  drupal_set_message($message, $type = 'error');
	  } else {
	  $message = t('FAQ updated');
	  drupal_set_message($message, $type = 'status');
	  }
	drupal_goto('admin/settings/minifaq/list'); 
	}	
	
function minifaq_list() {
	$header = array(t('FAQ'), t('Answer'), t('Edit'), t('Delete'));
	$results = db_query("SELECT * FROM {minifaq_faq} ORDER BY faqid ASC");
	while ($faqs = db_fetch_object($results)) {
		$deletepath = 'admin/settings/minifaq/delete/'.$faqs->faqid;
		$editpath = 'admin/settings/minifaq/edit/'.$faqs->faqid;
		$rows[] = array($faqs->question, $faqs->answer, l(t('Edit'),$editpath) , l(t('Delete'),$deletepath)) ;
		}	
	return theme('table', $header, $rows, array('id' => 'minifaq'));
}

function minifaq_delete($faqid = NULL) {	
	db_query("DELETE FROM {minifaq_faq} WHERE faqid = %d", $faqid);
	drupal_goto('admin/settings/minifaq/list');
}


function minifaq_block($op = 'list', $delta = 0, $edit = array()) {
  // The $op parameter determines what piece of information is being requested.
  switch ($op) {
    case 'list':
      // If $op is "list", we just need to return a list of block descriptions.
      // This is used to provide a list of possible blocks to the administrator,
      // end users will not see these descriptions.
	  $blocks[0] = array('info' => t('Frequently Asked Questions'),'weight' => -7, 'enabled' => 1, 'region' => 'left', 'status' => 1, 'visibility' => 1);
      return $blocks;
    case 'view':
      switch ($delta) {
        case 0:
          $block['subject'] = t('FAQs');
          $block['content'] = minifaq_block_list();
          break;
      }
      return $block;
  }
}

function minifaq_block_list(){
  $path = variable_get('minifaq_path','faqs');	
  $results = db_query("SELECT * FROM {minifaq_faq} ORDER BY faqid ASC");
  while ($faqs = db_fetch_object($results)) {
	$items[] = l($faqs->question, $path.'/'.$faqs->faqid);
	}	
  $output = theme('item_list', $items);	
  $output .= '<a class="more" href="/'.$path.'">More FAQs</a>';
  return $output;
}

function minifaq_page($faqid = NULL){
  $path = variable_get('minifaq_path','faqs');	
  if($faqid == NULL){
    $results = db_query("SELECT * FROM {minifaq_faq} ORDER BY faqid DESC");	
  }else{
    $results = db_query("SELECT * FROM {minifaq_faq} WHERE faqid = %d",$faqid);	
  }	
  while ($faqs = db_fetch_object($results)) {
	$items[$faqs->question] = $faqs->answer;
	}	
  $output = theme('minifaq_dl_list', $items);
  if(is_numeric(minifaq_lastarg())){
  	$output .= '<a class="more" href="/'.$path.'">More FAQs</a>';
  }	
  return $output;
}

function theme_minifaq_dl_list($items){
  $output = '<dl class="minifaq-dl-list">';
  foreach($items as $question => $answer){
    $output .= '<dt class="minifaq-dt">'.$question;
    $output .= '<dd class="minifaq-dd">'.$answer;
  }
  $output .= '</dl>';
  return $output;	
}

function minifaq_lastarg(){
	$args = explode('/', $_GET['q']);
    $argument = array_pop($args);
	return $argument;
}
